package org.apz.init;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apz.model.Ganster;
import org.apz.service.FileLoaderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class FileLoader implements ApplicationRunner {

    private static final Logger LOG = LoggerFactory.getLogger(FileLoader.class);
    
    @Value("${data.source}")
	private String dataSource; 
    
    @Autowired
	FileLoaderService fileLoaderService;
  
    List<String> fileErrors = new ArrayList<>();
    
    /**
     * Load mafia file to database
     */
    @Override
    public void run(final ApplicationArguments args) {
        if (fileLoaderService.count() == 0L) {
        	
        	try {
        		try (BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getClassLoader().getResourceAsStream(dataSource)))) {
        			Set<Ganster> gansters = br.lines().map(this::parseGanster)
				        	  .filter(Objects::nonNull)
				        	  .map(ganster -> fileLoaderService.load(ganster))
				        	  .collect(Collectors.toSet());
        			gansters.stream().filter(Ganster::isReleased).forEach(ganster -> fileLoaderService.addBoss(ganster));      			
                }
        		
                LOG.info("Mafia organization has {} gansters", fileLoaderService.count());
        		
			} catch (Exception e) {
				LOG.error("{}", e.getMessage());
			}
        	
            
        }
    }
    
     
    private Ganster parseGanster(String line) {
    	try {
    		String[] fields = line.split(";");
    		String id = getId(fields[0]);
    		String name = fields[1];
    		Date birthday = getBirthDay(fields[2]);
    		List<String> subordinates = getSubordinates(fields[3]);
        	Boolean released = Boolean.valueOf(fields[4]);
        	return new Ganster(id, name, released, birthday, subordinates);
		} catch (Exception e) {
			fileErrors.add(e.getMessage());
			return null;
		}
    	
    }
    
    
    private String getId(String gansterId) {
    	return String.format("%05d", Integer.valueOf(gansterId));
    }
    
    private Date getBirthDay(String gansterBirthday) throws ParseException {
    	return new SimpleDateFormat("dd/MM/yyyy").parse(gansterBirthday);
    }
	
	private List<String> getSubordinates(String subordinates) {
		return subordinates.isBlank() ? new ArrayList<String>() : Arrays.asList(subordinates.split(",")).stream().map(this::getId).collect(Collectors.toList());
	}
	
}
