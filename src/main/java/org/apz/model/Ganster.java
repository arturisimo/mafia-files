package org.apz.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "gansters")
public class Ganster implements Serializable {
	
	private static final long serialVersionUID = 4939759278878349665L;
	
	@Id
	private String id;
	
	@Indexed(unique=true)
	private String name;
	
	private Ganster boss;

	private Boolean released;
	
	private Date birthday;
	
	private List<String> subordinates;

	public Ganster(String id, String name, Boolean released, Date birthday, List<String> subordinates) {
	     super();
	     this.id = id;
	     this.name = name;
	     this.released = released;
	     this.birthday = birthday;
	     this.subordinates = subordinates;
	}
	
    public Ganster() {
		super();
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Ganster getBoss() {
        return this.boss;
    }
	public void setBoss(Ganster boss) {
		this.boss = boss;
	}
	
    public Boolean isReleased() {
		return released;
	}

	public void setReleased(Boolean released) {
		this.released = released;
	}
	
	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public List<String> getSubordinates() {
		return subordinates;
	}

	public void setSubordinates(List<String> subordinates) {
		this.subordinates = subordinates;
	}

	@Override
	public String toString() {
		return "Ganster [id=" + id + ", name=" + name + ", boss=" + boss + ", released=" + released + ", birthday="
				+ birthday + ", subordinates=" + subordinates + "]";
	}
	
}
