package org.apz.service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apz.model.Ganster;
import org.apz.repository.GansterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FileLoaderServiceImpl implements FileLoaderService {
	
	@Autowired
	GansterRepository gansterRepository; 
	
	
	@Override
	public Long count() {
		return gansterRepository.count();
	}

	@Override
	public Ganster load(Ganster ganster) {
		return gansterRepository.save(ganster);
	}


	@Override
	public void addBoss(Ganster ganster) {
		getSubordinates(ganster).forEach(subordinate -> {
			subordinate.setBoss(ganster);
			gansterRepository.save(subordinate);
		});
	}
	
	private List<Ganster> getSubordinates(Ganster ganster) {
		return ganster.getSubordinates().stream().map(id -> gansterRepository.findById(id).orElse(null))
												 .filter(Objects::nonNull)
												 .collect(Collectors.toList());
	}
	
}