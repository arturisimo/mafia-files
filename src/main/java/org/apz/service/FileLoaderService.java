package org.apz.service;

import org.apz.model.Ganster;

public interface FileLoaderService {

	Long count();

	Ganster load(Ganster ganster);

	void addBoss(Ganster ganster);
	

}
