package org.apz.service;

import java.util.List;

import org.apz.dto.GansterDto;
import org.apz.dto.Node;

public interface GansterService {

	/**
	 * List all organization
	 * @return
	 * @throws Exception
	 */
	List<Node> listAll() throws Exception;
	
	/**
	 * Detail of ganster
	 * @param id
	 * @return
	 * @throws Exception
	 */
	Node findById(String id) throws Exception;
	
	/**
	 * release/imprison ganster
	 * @param id
	 * @throws Exception
	 */
	GansterDto changeStatus(String id) throws Exception;

	void delete(String id);
}

