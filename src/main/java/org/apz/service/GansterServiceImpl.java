package org.apz.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apz.dto.GansterDto;
import org.apz.dto.Node;
import org.apz.model.Ganster;
//import org.apz.model.Node;
import org.apz.repository.GansterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class GansterServiceImpl implements GansterService {
	
	private static final Logger LOG = LoggerFactory.getLogger(GansterServiceImpl.class);
	
	@Autowired
	GansterRepository gansterRepository; 
	
	Map<String, Ganster> cacheGansters;
	
	@Value("${ganster.special.surveillance.subordinates}")
	private int specialSurveillance; 
	
	
	@Override
	public List<Node> listAll() throws Exception {
		populateCache(false);
		return cacheGansters.values().stream().map(g -> getNode(g))
											  .sorted(Comparator.comparing(n -> n.getGanster().getId()))
											  .collect(Collectors.toList());
			
	}
	
	@Override
	public Node findById(String id) throws Exception {
		populateCache(false);
    	Ganster ganster = cacheGansters.get(id);
    	return getNode(ganster);
	}
	
	@Override
	public GansterDto changeStatus(String id) throws Exception {
		Ganster ganster = cacheGansters.get(id);
		
		if (ganster.isReleased()) {
			imprison(ganster);
		} else {
			release(ganster);
		}
		
		//update ganster
		ganster.setReleased(!ganster.isReleased());
		gansterRepository.save(ganster);
		populateCache(true);
		return mapGanster(ganster);
	}
	
	/**
	 * All his direct subordinates are immediately relocated and now work for the oldest remaining boss at the same level as their previous boss. 
	 * If there is no such possible alternative boss the oldest direct subordinate of the previous boss is promoted to be the boss of the others.
	 * @param ganster
	 */
	private void imprison(Ganster ganster) {
		LOG.info("imprison ganster: {} {}", ganster.getId(), ganster.getName());
		Ganster newBoss = getNewBoss(ganster);
		relocateSubordinates(ganster, newBoss);
	}
	
	private void release(Ganster ganster) throws Exception {
		LOG.info("release ganster: {} {}", ganster.getId(), ganster.getName());
		recoverSubordinates(ganster);
	}
	
	private void relocateSubordinates(Ganster oldBoss, Ganster ganster) {
		ganster.getSubordinates().addAll(oldBoss.getSubordinates());
		gansterRepository.save(ganster);
		oldBoss.setSubordinates(new ArrayList<>());
		gansterRepository.save(oldBoss);
		LOG.info("relocateSubordinates: {}", ganster.getSubordinates());
	}
	
	private void recoverSubordinates(Ganster boss) throws Exception {
		List<Ganster> subordinates = cacheGansters.values().stream().filter(g -> g.getBoss()!=null).filter(g -> g.getBoss().getId().equals(boss.getId())).collect(Collectors.toList());
		if (subordinates.size() > 0) {
			List<String> idSubordinates = subordinates.stream().map(Ganster::getId).collect(Collectors.toList());
			Ganster oldBoss = cacheGansters.values().stream().filter(g -> g.getSubordinates().contains(idSubordinates.get(0))).findFirst().orElseThrow(() -> new Exception("error")) ;
			oldBoss.getSubordinates().removeAll(idSubordinates);
			gansterRepository.save(oldBoss);
			boss.setSubordinates(idSubordinates);
			gansterRepository.save(boss);
			LOG.info("recoverSubordinates: {}", idSubordinates);
		}
	}
	
	private Ganster getNewBoss(Ganster ganster) {
		
		Ganster newBoss = getOldestSameLevelGanster(ganster);
		
		if (newBoss == null) {
			newBoss = getPromotedSubordinate(ganster.getSubordinates());
		}
		LOG.info("new boss: {} {}", newBoss.getId(), newBoss.getName());
		return newBoss;
	}
	
	
	/**
	 * Oldest remaining boss at the same level as their previous boss.
	 * @param idGansters
	 * @param excludeId
	 * @return
	 */
	private Ganster getOldestSameLevelGanster(Ganster ganster) {
		if (ganster.getBoss() == null)
			return null;
		
		return ganster.getBoss().getSubordinates().stream()
												  .filter(id -> !id.equals(ganster.getId()))
												  .map(id -> cacheGansters.get(id))
												  .filter(Ganster::isReleased)
												  .findFirst().orElse(null);
		
	}
	
	
	
	/**
	 * Oldest direct subordinate of the previous boss is promoted to be the boss of the others
	 * @param idGansters
	 * @return
	 */
	private Ganster getPromotedSubordinate(List<String> idGansters) {
		return idGansters.stream()
						  .map(id -> cacheGansters.get(id))
						  .filter(Ganster::isReleased)
						  .findFirst().orElse(null);
		
	}

	
	private Set<Ganster> getSubordinates(Ganster ganster) {
		return ganster.getSubordinates().stream()
												 .filter(s-> !s.equals(ganster.getId()))//avoid cyclic callings
												 .map(id -> cacheGansters.get(id))
												 .collect(Collectors.toSet());
	}
	
	private Node getNode(Ganster ganster) {
		GansterDto gansterDto = mapGanster(ganster);
		Node node = new Node(gansterDto, specialSurveillance);
		for (Ganster subordinate : getSubordinates(ganster) ) {
			node.addSubordinate(getNode(subordinate));
		}
		return node;
	}
	
	
	private GansterDto mapGanster(Ganster ganster) {
		return new GansterDto(ganster.getId(), ganster.getName(), ganster.isReleased(), ganster.getBirthday());
	}
	
	
	private void populateCache(boolean force) {
		try {
			LOG.error("init cache mafia");
			if (cacheGansters == null || force) {
				List<Ganster> gansters = gansterRepository.findAll();
				cacheGansters = gansters.stream().collect(Collectors.toMap(g -> g.getId(), g -> g));
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
	}
	
	@Override
	public void delete(String id) {
		gansterRepository.deleteById(id);
	}
	
	
}
	
