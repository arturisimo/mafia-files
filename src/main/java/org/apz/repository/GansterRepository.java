package org.apz.repository;

import java.util.Optional;

import org.apz.model.Ganster;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GansterRepository extends MongoRepository<Ganster, String> {

	Optional<Ganster> findByName(String name);

}
