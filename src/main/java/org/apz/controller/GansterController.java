package org.apz.controller;

import static org.springframework.web.servlet.function.RouterFunctions.route;

import org.apz.handler.GansterHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.function.RouterFunction;
import org.springframework.web.servlet.function.ServerResponse;

@Controller
public class GansterController {
	
	@Bean
	public RouterFunction<ServerResponse> quoteRouter(GansterHandler handler) {
			
			return route().GET("/mafia", handler::index)
						  .GET("/todo", handler::todo)
						  .GET("/detail/{id}", handler::detail)
						  .PUT("/release/{id}", handler::release)
						  .DELETE("/delete/{id}", handler::delete)
						  .build();
		
	}
		
	
	
	
}
