package org.apz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MafiaFilesApplication {

	public static void main(String[] args) {
		SpringApplication.run(MafiaFilesApplication.class, args);
	}
	
}
