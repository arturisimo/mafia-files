package org.apz.handler;

import java.util.HashMap;
import java.util.Map;

import org.apz.dto.GansterDto;
import org.apz.service.GansterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.function.ServerRequest;
import org.springframework.web.servlet.function.ServerResponse;

@Component
public class GansterHandler {
	
	@Autowired
	GansterService gansterService;
	
	private static final Logger LOG = LoggerFactory.getLogger(GansterHandler.class);
	
	public ServerResponse index(ServerRequest request) {
		Map<String, Object> data = new HashMap<>();
		try {
			data.put("gansters", gansterService.listAll());
			return ServerResponse.ok().contentType(MediaType.TEXT_HTML).render("index", data);
		} catch (Exception e) {
			LOG.error(e.getMessage());
			data.put("error", e.getMessage());
			return ServerResponse.ok().contentType(MediaType.TEXT_HTML).render("error", data);
		}
	}
	
	public ServerResponse todo(ServerRequest request) {
		return ServerResponse.ok().contentType(MediaType.TEXT_HTML).render("todo");
	}
	
	public ServerResponse detail(ServerRequest request) {
		String id = request.pathVariable("id");
		Map<String, Object> data = new HashMap<>();
		
		try {
			data.put("ganster", gansterService.findById(id));
			data.put("edit", false);
			return ServerResponse.ok().contentType(MediaType.TEXT_HTML).render("detail", data);
		} catch (Exception e) {
			LOG.error(e.getMessage());
			data.put("error", e.getMessage());
			return ServerResponse.ok().contentType(MediaType.TEXT_HTML).render("error", data);
		}
	
	}
	
	public ServerResponse release(ServerRequest request) {
		String id = request.pathVariable("id");
		try {
			GansterDto ganster = gansterService.changeStatus(id);
			return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(ganster);
			
		} catch (Exception e) {
			LOG.error(e.getMessage());
			return ServerResponse.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{'message': '"+ e.getMessage() + "'}");
		}
		
	}
	
	public ServerResponse delete(ServerRequest request) {
		String id = request.pathVariable("id");
		try {
			gansterService.delete(id);
			return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(null);
			
		} catch (Exception e) {
			LOG.error(e.getMessage());
			return ServerResponse.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{'message': '"+ e.getMessage() + "'}");
		}
	}
	
}
