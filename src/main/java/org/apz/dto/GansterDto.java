package org.apz.dto;

import java.io.Serializable;
import java.util.Date;

public class GansterDto implements Serializable {
		
	private static final long serialVersionUID = -1514882401378836425L;

	private String id;
	
	private String name;
	
	private Boolean released;
	
	private Date birthday;
	
	public GansterDto(String id, String name, Boolean released, Date birthday) {
	     super();
	     this.id = id;
	     this.name = name;
	     this.released = released;
	     this.birthday = birthday;
	}
	
    public GansterDto() {
		super();
	}

	public String getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
    public Boolean isReleased() {
		return released;
	}

	
	public Date getBirthday() {
		return birthday;
	}

	
	@Override
	public String toString() {
		return "GansterDto [id=" + id + ", name=" + name + ", released=" + released + ", birthday=" + birthday
				+ "]";
	}

	
}
