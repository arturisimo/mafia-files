package org.apz.dto;

import java.util.HashSet;
import java.util.Set;

public class Node {
	
	private GansterDto ganster;
    private Set<Node> subordinates;
    private Node boss;
    
    private int specialSurveillance;
    
    public Node() {
    }
    
    public Node(GansterDto ganster, int specialSurveillance) {
        this.ganster = ganster;
        this.specialSurveillance = specialSurveillance;
        this.subordinates = new HashSet<>();
    }
    
    public GansterDto getGanster() {
		return this.ganster;
	}
    
    public void setGanster(GansterDto ganster) {
        this.ganster = ganster;
    }

    public Node getBoss() {
        return this.boss;
    }

    public void setBoss(Node boss) {
        this.boss = boss;
    }

    public Set<Node> getSubordinates() {
        return this.subordinates;
    }
    
    public void addSubordinate(Node subordinate) {
    	subordinate.setBoss(this);
        subordinates.add(subordinate);
    }
    
    
    public void setSubordinates(Set<Node> subordinates) {
        for (Node subordinate : subordinates)
        	subordinate.setBoss(this);

        this.subordinates = subordinates;
    }
    
    public boolean isSpecialSurveillance() {
    	return getNumberOfSubordinates() > specialSurveillance;
    }
    
    @Override
	public String toString() {
		return "Ganster [ganster=" + ganster + " total=" + getNumberOfSubordinates() +"]";
	}

	public int getNumberOfSubordinates() {
        return auxiliaryGetNumberOfSubordinates(this);
    }

    private int auxiliaryGetNumberOfSubordinates(Node ganster) {
    	
    	int numberOfNodes = ganster.getSubordinates().size();
        
        for(Node subordinate : ganster.getSubordinates()) {
            numberOfNodes += subordinate.getGanster().isReleased() ? auxiliaryGetNumberOfSubordinates(subordinate) : 0;
        }
        return numberOfNodes;
    }
	
	
}
