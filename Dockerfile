FROM openjdk:11

ADD target/mafia-files-0.0.1-SNAPSHOT.jar app.jar

ENTRYPOINT ["java","-jar","/app.jar"]
CMD ["/bin/bash"]